message '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
message '<      HXT_Editor_V001.asm     >'
        #include HXT_Editor_V001.INC
        PUBLIC  _HXT_Editor_V001
        PUBLIC  _HXT_Editor_V001_INITIAL


;*******************************************************************
#define CMD_R_Lib_VersionL  00H
#define CMD_R_Lib_VersionH  01H
#define CMD_R_KeyAmount 02H
#define CMD_R_EE_Status 03H
#define CMD_R_TK_Select0    04H
#define CMD_R_KeyStatus0    08H
#define CMD_R_K1_Freq   010H
#define CMD_R_K1_Ref    050H
#define CMD_R_K1_Offset 090H
#define CMD_R_K1_RCC    0B0H

#define CMD_RW_Global   0D0H
#define CMD_RW_DeviceA  CMD_RW_Global+0
#define CMD_RW_DeviceB  CMD_RW_Global+1
#define CMD_RW_KeyAmount    CMD_RW_Global+2
#define CMD_RW_OptionA  CMD_RW_Global+3
#define CMD_RW_OptionB  CMD_RW_Global+4
#define CMD_RW_OptionC  CMD_RW_Global+5
;#define    CMD_RW_     CMD_RW_Global+6
;#define    CMD_RW_     CMD_RW_Global+7
#define CMD_RW_K1_TH    CMD_RW_Global+8


;;;;;#define    CMD_R_Lib_VersionL  00H
;;;;;#define    CMD_R_Lib_VersionH  01H
;;;;;#define    CMD_R_KeyAmount 02H
;;;;;#define    CMD_R_EE_Status 03H
;;;;;#define    CMD_R_TK_Select0    04H
;;;;;#define    CMD_R_KeyStatus0    08H
;;;;;#define    CMD_R_K1_Freq   010H
;;;;;#define    CMD_R_K1_Ref    050H
;;;;;#define    CMD_R_K1_RCC    090H
;;;;;
;;;;;#define    CMD_RW_Global   0B0H
;;;;;#define    CMD_RW_B0       CMD_RW_Global+0
;;;;;#define    CMD_RW_B1       CMD_RW_Global+1
;;;;;#define    CMD_RW_OptionA  CMD_RW_Global+2
;;;;;#define    CMD_RW_OptionB  CMD_RW_Global+3
;;;;;#define    CMD_RW_OptionC  CMD_RW_Global+4
;;;;;#define    CMD_RW_K1_TH    CMD_RW_Global+5
;*********************************************************************





;;;;;;*******************************************************************
RAMBANK 0 IIC_SLAVE_TEST_RW_RAM
IIC_SLAVE_TEST_RW_RAM       .SECTION  'DATA'
IIC_STACK   DB 2 DUP(?)
;*******************************************************************
IIC_SLAVE_TEST_RW_CODE    .SECTION  'CODE'
;;;;;IIC_WRITE_ENTRY PROC
;;;;;        JMP END_IIC_RW
;;;;;IIC_WRITE_ENTRY ENDP
;================================================================

;*******************************************************************

;*******************************************************************
IIC_PRE_READ PROC
        MOV     A,MP1
        MOV     IIC_STACK[0],A
        MOV     A,BP
        MOV     IIC_STACK[1],A
        CLR     IIC_DATA_OUT
;================================================================

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_RW_K1_TH
        SZ      C
        JMP     IIC_READ_KEY_TH

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_RW_OptionA        ;CMD_RW_Global
        SZ      C
        JMP     IIC_READ_Setting

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_K1_RCC
        SZ      C
        JMP     IIC_READ_RCC

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_K1_OFFSET
        SZ      C
        JMP     END_IIC_RW

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_K1_Ref
        SZ      C
        JMP     IIC_READ_REF

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_K1_Freq
        SZ      C
        JMP     IIC_READ_FREQ

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_KeyStatus0
        SZ      C
        JMP     IIC_READ_STATUS

        MOV     A,IIC_DATA_INDEX
        SUB     A,CMD_R_TK_Select0
        SZ      C
        JMP     IIC_READ_TK_SEL

        MOV     A,IIC_DATA_INDEX
        XOR     A,CMD_R_EE_Status
        SNZ     Z
        JMP     $+4
        MOV     A,0
        MOV     IIC_DATA_OUT,A
        JMP     END_IIC_RW

        MOV     A,IIC_DATA_INDEX
        XOR     A,CMD_R_KeyAmount
        SNZ     Z
        JMP     $0
        MOV     A,HXT_KeyAmount
        MOV     IIC_DATA_OUT,A
        JMP     END_IIC_RW
    $0:
        MOV     A,IIC_DATA_INDEX
        XOR     A,CMD_R_Lib_VersionH
        SNZ     Z
        JMP     $1
        MOV     A,IIC_LibVer[1]
        MOV     IIC_DATA_OUT,A
        JMP     END_IIC_RW
    $1:
        SZ      IIC_DATA_INDEX
        JMP     END_IIC_RW
        MOV     A,IIC_LibVer[0]
        MOV     IIC_DATA_OUT,A
        JMP     END_IIC_RW
    IIC_READ_KEY_TH:
                ;ADD     A,OFFSET        _GLOBE_VARIES+3
                ADD     A,OFFSET        _KEY_THR
                MOV     MP1,A
                ;MOV     A,BANK  _GLOBE_VARIES
                MOV     A,BANK _KEY_THR
                MOV     BP,A
                JMP     IIC_PRELOAD_DATA
    IIC_READ_Setting:
                ADD     A,OFFSET        _GLOBE_VARIES
                MOV     MP1,A
                MOV     A,BANK  _GLOBE_VARIES
                MOV     BP,A
                JMP     IIC_PRELOAD_DATA
    IIC_READ_STATUS:
                ADD     A,OFFSET    _KEY_DATA
                MOV     MP1,A
                MOV     A,BANK  _KEY_DATA
                MOV     BP,A
                JMP     IIC_PRELOAD_DATA
    IIC_READ_TK_SEL:
                ADD     A,OFFSET    _KEY_IO_SEL
                MOV     MP1,A
                MOV     A,BANK  _KEY_IO_SEL
                MOV     BP,A

                MOV     A,IAR1
                XOR     A,0FFH
                MOV     IIC_DATA_OUT,A
                JMP     END_IIC_RW

    IIC_READ_RCC:
                RL      ACC
                RL      ACC
                ADD     A,OFFSET    _KEY_REF + 2 ;3M CAP
                MOV     MP1,A
                MOV     A,BANK  _KEY_REF
                MOV     BP,A
                JMP     IIC_PRELOAD_DATA
                    ;----------------
    IIC_READ_FREQ:  ;-READ KEY FREQ -
                    ;----------------
                RL      ACC
                AND     A,11111100B
                ADD     A,OFFSET    _KEY_REF + 1
                MOV     MP1,A
                MOV     A,BANK  _KEY_REF
                MOV     BP,A
                JMP     IIC_READ_FREQ_REF
                    ;----------------
    IIC_READ_REF:   ;-READ REFERENCE-
                    ;----------------
                RL      ACC
                AND     A,11111100B
                ADD     A,OFFSET    _KEY_REF
                MOV     MP1,A
                MOV     A,BANK  _KEY_REF
                MOV     BP,A
        IIC_READ_FREQ_REF:
                SZ      IIC_DATA_INDEX.0
                JMP     IIC_READ_FREQ_REF_HIGH_BYTE
                MOV     A,IAR1
                MOV     IIC_DATA_OUT,A
                JMP END_IIC_RW
        IIC_READ_FREQ_REF_HIGH_BYTE:
                CLR     IIC_DATA_OUT
                JMP     END_IIC_RW
    IIC_PRELOAD_DATA:;-IIC PRE-LOAD DATA
                MOV     A,IAR1
                MOV     IIC_DATA_OUT,A
;================================================================
END_IIC_RW:
        MOV     A,IIC_STACK[1]
        MOV     BP,A
        MOV     A,IIC_STACK[0]
        MOV     MP1,A
        JMP     _ENDIIC
IIC_PRE_READ ENDP
;*******************************************************************















































;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************
;*************************************************************************************************************************************************



RAMBANK 0 HXT_Editor_V001_RAM
HXT_Editor_V001_RAM       .SECTION  'DATA'
ifdef   PBP
IIC_INT_STACK       DB 3 DUP(?)
else
IIC_INT_STACK       DB 2 DUP(?)
endif
bWrite1st       DBIT
IIC_DATA_OUT    DB      ?
IIC_DATA_IN DB      ?
IIC_DATA_INDEX  DB      ?
IIC_LibVer  DB  2   DUP(?)
;=====================================================================





;*********************************************************************
#define SlaveMode       HTX
#define AddrMatch       HAAS
#define MasterRead  SRW
#define SlaveRecAck RXAK
DUMMY_READ  macro
        MOV A,IIC_DATA      ;DUMMY READ
        endm
SLAVE_SEND_ACK  macro           ;enable transmits acknowledge
        CLR TXAK
        endm
Slave_Tx_Mode   macro
        set SlaveMode
        endm
Slave_Rx_Mode   macro           ;receive mode (master write)
        clr SlaveMode
        endm

IF_AddrNotMatch_GOTO    macro   addr
        SNZ AddrMatch       ;0:read/write data 1:address match
        JMP addr
        endm

IF_WriteData_GOTO   macro   addr
        SNZ MasterRead  ;0:master wants to write data 1:master wants to read data
        JMP addr
        endm

IF_Master_Read_GOTO macro   addr
        SZ  SlaveMode
        JMP addr
        endm
IF_ACK_GOTO macro   addr
        SNZ SlaveRecAck
        JMP addr
        endm

;*********************************************************************
IIC_INT_CODE   .SECTION  AT IIC_INT_ADDR 'CODE'
                MOV     IIC_INT_STACK[0],A
        ifdef   PBP
                MOV     A,PBP
                CLR     PBP
        endif
                JMP     IIC_INT

HXT_Editor_V001_CODE    .SECTION  'CODE'
IIC_INITIAL PROC
;JMP    IIC_WRITE_ENTRY
;jmp    IIC_PRE_READ

        ;ifdef   IIC_SDA_PU
        ;        SET     IIC_SDA_PU
        ;endif
        ;ifdef   IIC_SCL_PU
        ;        SET     IIC_SCL_PU
        ;endif
        IIC_SDA_PU
        IIC_SCL_PU
        IIC_SDA_PIN
        IIC_SCL_PIN

        ;--
        MOV     A,11000010B ;I2C Mode ,Enable SIM
        MOV     IIC_INT_CTRL,A


        MOV     A,IIC_ADDRESS
        MOV     IIC_ADDR,A

        CLR     HTX
        CLR     TXAK
        SET     IIC_ADDR_MATCH_WAKEUP
        ;--
        CLR     IIC_INT_FLG        ;Clear SIM interrupt request flag

        MOV     A,10111111B ;Enable I2C Time-out Countrol
        MOV     IIC_TimeOutCtrl,A              ;I2C time-out time is 64*32/32K = 62.5mSec

        SET     IIC_INT_ENABLE

        RET
IIC_INITIAL ENDP







;*******************************************************************
_HXT_Editor_V001:
        SZ      IIC_TimeOut
        CALL    IIC_INITIAL
        RET
;*******************************************************************


;*******************************************************************
_HXT_Editor_V001_INITIAL:
        CALL    IIC_INITIAL
        CALL    _GET_LIB_VER
        MOV     IIC_LibVer[1],A
        MOV     A,_DATA_BUF[0]
        MOV     IIC_LibVer[0],A
        RET
;*******************************************************************












;;***********************************************************
;;*SUB. NAME:                                               *
;;*INPUT    :                                               *
;;*OUTPUT   :                                               *
;;*USED REG.:                                               *
;;*FUNCTION :                                               *
;;***********************************************************
                ;;*******************
IIC_INT:        ;;* IIC INT. ********
                ;;*******************
        ifdef  PBP
                MOV     IIC_INT_STACK[2],A
        endif
                MOV     A,STATUS
                MOV     IIC_INT_STACK[1],A

        ifdef   IIC_IS_MF_INT
                SNZ     IIC_INT_FLG
                JMP     _ENDIIC
                CLR     IIC_INT_FLG
        endif


                ;-CHECK IF ADDRESS MATCH
                IF_AddrNotMatch_GOTO    IIC_CONTINUE_RW
                IF_WriteData_GOTO   FIRST_WRITE
                ;-FIRST READ (PREPARE DATA)
                Slave_Tx_Mode
                JMP     IIC_CONTINUE_READ
    IIC_CONTINUE_RW:
                IF_Master_Read_GOTO IIC_MASTER_READ
                ;-MASTER WRITE
                MOV     A,IIC_DATA
                MOV     IIC_DATA_IN,A
                SZ      bWrite1st
;;;;;           JMP IIC_WRITE_ENTRY
                JMP     _ENDIIC

                MOV     IIC_DATA_INDEX,A    ;-GET 1ST BYTE (ADDRESS)
                SET     bWrite1st
                JMP     IIC_PRE_READ    ;PR-READ DATA
    FIRST_WRITE:
                CLR     bWrite1st
                JMP     SIM_RECEIVE_MODE
    IIC_MASTER_READ:
                IF_ACK_GOTO IIC_CONTINUE_READ
                ;NACK
    SIM_RECEIVE_MODE:
                Slave_Rx_Mode       ;receive mode (master write)
                SLAVE_SEND_ACK      ;enable transmits acknowledge
                DUMMY_READ
    _ENDIIC:
        ifdef   PBP
                MOV     A,IIC_INT_STACK[2]
                MOV     PBP,A
        endif
                MOV     A,IIC_INT_STACK[1]
                MOV     STATUS,A
                MOV     A,IIC_INT_STACK[0]
                RETI
;*******************************************************************
IIC_CONTINUE_READ:
        MOV     A,IIC_DATA_OUT
        MOV     IIC_DATA,A
        INC     IIC_DATA_INDEX
        JMP     IIC_PRE_READ
;*******************************************************************
message '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '
