/******************************************************************************/
#ifndef	_MY_TYPE_H_
#define	_MY_TYPE_H_



typedef    unsigned char		u8;
typedef    unsigned int 	 	u16;
typedef    unsigned long 	 	u32;	
typedef    unsigned long long 	u64;

typedef    signed char			s8;
typedef    signed int 	 		s16;
typedef    signed long 	 		s32;	
typedef    signed long long 	s64;

typedef    unsigned char		uint8;
typedef    unsigned int 	 	uint16;
typedef    unsigned long 	 	uint32;	
typedef    unsigned long long 	uint64;

typedef    signed char			int8;
typedef    signed int 	 		int16;
typedef    signed long 	 		int32;	
typedef    signed long long 	int64;


#define TRUE    (1)
#define FALSE   (0)

#define ENABLE    (1)
#define DISABLE   (0)

#define ON						1
#define OFF						0


typedef struct {
unsigned char bit0 : 1;
unsigned char bit1 : 1;
unsigned char bit2 : 1;
unsigned char bit3 : 1;
unsigned char bit4 : 1;
unsigned char bit5 : 1;
unsigned char bit6 : 1;
unsigned char bit7 : 1;
}bit_type;


#endif
/******************************************************************************/