//___________________________________________________________________
//___________________________________________________________________
//  Copyright : 2015 BY HOLTEK SEMICONDUCTOR INC
//  File Name : EEPROM.h
// Description: EEPROM�x���ӳ���
//   Customer : Holtek Demo Code
//Targer Board: Holtek Demo Board
//   MCU      : None
//___________________________________________________________________
//___________________________________________________________________
#ifndef _EEPROM_H__
#define _EEPROM_H__
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Userdefine @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ���ú��� @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
unsigned char EEPROM_ReadByte(unsigned char addr);
void EEPROM_WriteByte(unsigned char addr, unsigned char WriteData);
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  �A̎��  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
typedef struct {
	    unsigned char bit0:1;
	    unsigned char bit1:1;
	    unsigned char bit2:1;
	    unsigned char bit3:1;
	    unsigned char bit4:1;
	    unsigned char bit5:1;
	    unsigned char bit6:1;
	    unsigned char bit7:1;
	} iar_bits;
DEFINE_SFR(iar_bits,iar1,0x02);
#define	_EEPROM_WREN	iar1.bit3
#define	_EEPROM_WR		iar1.bit2
#define	_EEPROM_RDEN	iar1.bit1
#define	_EEEPROM_RD		iar1.bit0


#endif
