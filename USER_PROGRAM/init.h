#ifndef  _INIT_H_
#define  _INIT_H_

#define ledcom			  _pd7
#define ledcom_c			_pdc7
#define led_1			    _pd6
#define led_1_c			  _pdc6
#define led_2			    _pd5
#define led_2_c			  _pdc5
#define led_3			    _pd4
#define led_3_c			  _pdc4
#define led_ro		    _pd3
#define led_ro_c		  _pdc3
#define led_ns		    _pd2
#define led_ns_c		  _pdc2
#define led_rst			  _pa1
#define led_rst_c			_pac1
#define led_next			_pa3
#define led_next_c		_pac3

#define buzzer        _pa0
#define buzzer_c      _pac0

#define pump          _pd1
#define pump_c        _pdc1

#define flow          _pd0
#define flow_c        _pdc0


void IOinit();
void timerInit();
void ledInit();
void ledAllOn();
void ledAllOff();


#endif