//___________________________________________________________________
//___________________________________________________________________
//  Copyright : 2015 BY HOLTEK SEMICONDUCTOR INC
//  File Name : EEPROM.c
// Description: EEPROM�x��
//   Customer : Holtek Demo Code
//Targer Board: None
//     MCU    : None
//___________________________________________________________________
//___________________________________________________________________
#include "include.h"
/********************************************************************
Function: EEPROM_ReadByte
INPUT	: @addr �x��ַ
OUTPUT	: @1Word ����
NOTE	: none
*******************************************************************/
unsigned char EEPROM_ReadByte(unsigned char addr)
{
	u8 gu8v_EEPROM_RWData;
	_eea  = addr;
	_mp1h = 1;
	_mp1l = 0x40;
	_EEPROM_RDEN = 1;				// Enable READ
	_EEEPROM_RD  = 1;				// Active READ
	while(_EEEPROM_RD);				//wait read end
	gu8v_EEPROM_RWData = _eed;
	_iar1=0;
	_mp1h=0;
	return gu8v_EEPROM_RWData;
}
/********************************************************************
Function: EEPROM_WriteByte
INPUT	: @addr ����ַ @gu16v_EEPROM_RWData ������
OUTPUT	: None
NOTE	: none
*******************************************************************/
void EEPROM_WriteByte(unsigned char addr, unsigned char WriteData)
{
	u8 gu8v_EEPROM_RWData;
	gu8v_EEPROM_RWData = WriteData;
	unsigned char EMI_Protect=0;
	EMI_Protect = _emi;
	_emi  = 0;
	_mp1h = 1;
	_mp1l = 0x40;
	_eea  = addr;
	_eed  = gu8v_EEPROM_RWData;
	asm("set [0x02].3");
	asm("set [0x02].2");
	_emi = EMI_Protect;
	while((_EEPROM_WR));		//wait write end
	_iar1=0;					//disable WREN
	_mp1h=0;
	
}