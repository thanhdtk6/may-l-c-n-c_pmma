#ifndef  _KEY_SCAN_H_
#define  _KEY_SCAN_H_

// bit index button
#define BUTTON_1_B 0 // button reset
#define BUTTON_2_B 1 // button next

#define BTN_RST BUTTON_1_B
#define BTN_NXT BUTTON_2_B

#define DEBOUNCE_TIME 2      // 20ms
#define LONGPRESS_TIME 300   // 3s
#define TRIPPLE_TIME_MAX 200 // 2s

#define NULL 0

void initButton();
void button_scan();
u16 button_wasPressed();
u16 button_wasReleased();
u16 button_longPress();
u16 button_tripplePress();


#endif