#ifndef  _USER_VARIABLE_H_
#define  _USER_VARIABLE_H_

//cook mode
#define	MODE_NONE           0
#define MODE_HOTPOT         1   // lẩu
#define MODE_GRILL          2   // xào
#define MODE_SOUP           3   // súp
#define MODE_PORRIDGE       4   // nấu cháo
#define MODE_WARM_MILK      5   // hâm sữa
#define MODE_STEAMING       6   // hấp
#define MODE_KEEP_WARM      7   // giữ ấm
#define MODE_FRYING         8   // nướng


//cook mode
#define  STATUS_LOCK    1
#define  STATUS_UNLOCK  0   
#define	 STATUS_ON      1
#define	 STATUS_OFF     0 	    
#define	 STATUS_PAUSE   1  	    
#define	 STATUS_TIMER   1  
#define	 STATUS_NOMAL   0

typedef enum {
  PRESS_STATE = 0,
  NOTPRESS_STATE,
  HOLD_STATE
} ButtonState_TypeDef;

typedef enum {
  DP_MODE_POWER = 0,
  DP_MODE_TEMPERATURE,
  DP_MODE_TIMER
} DisplayMode_TypeDef;







#endif