#include "INCLUDE.h"



void IOinit()
{
	_acerl=0;
	ledcom_c=0;
	led_1_c=0;
	led_2_c=0;
  led_3_c=0;
  led_ro_c=0;
  led_ns_c=0;
  led_rst_c=0;
  led_next_c=0;
  buzzer_c = 0;
  pump_c = 1; // pump is input
  flow_c = 1; // flow is input

	_papu=0;
	_pbpu=0;
	_pcpu=0;
	_pdpu=0;
	_tkm0c1=00000000;
	_tkm1c1=00000000;
	_tkm2c1=00000000;		//unable touch key9/10/11/12=>pin11/12/13/14
	_tmpc=0;
  _pdpu0 = 1; // enable pull-up at pd0
}


void timerInit()  //125us
{
	_ctm0c0=0x00;//fsys = 16mhz
	_ctm0c1=0xc1;//counter mode  && compare A enable 
	_ctm0al=500&0xff;
	_ctm0ah=500>>8;
	_ctm0ae=1;
	_ct0on=1;
}

void ledInit()
{
	ledcom = 0;
	led_1=0;
	led_2=0;
  led_3=0;
  led_ro=0;
  led_ns=0;
  led_rst=0;
  led_next=0;
}

void ledAllOn()
{
	led_1=1;
	led_2=1;
  led_3=1;
  led_ro=1;
  led_ns=1;
  led_rst=1;
  led_next=1;
}

void ledAllOff()
{
	led_1=0;
	led_2=0;
  led_3=0;
  led_ro=0;
  led_ns=0;
  led_rst=0;
  led_next=0;
}