#include "INCLUDE.h"

// 10ms scan interval

volatile u16 button_state; // the state of button

volatile u16 lastButton_state;
volatile u16 buttonWasPress;
volatile u16 buttonWasReleased;
volatile u16 buttonLongPress;
volatile u16 buttonTripplePress;

volatile u16 button_time[2];
volatile u16 checkTripple_time[2];
volatile u16 release_cnt[2];
volatile u16 release_4multi_cnt[2];

void initButton() {
  u8 i;

  button_state = 0;
  lastButton_state = 0;
  buttonWasPress = 0;
  buttonWasReleased = 0;
  buttonLongPress = 0;
  buttonTripplePress = 0;

  for (i = 0; i < 2; i++) {
    button_time[i] = 0;
    checkTripple_time[i] = 0;
    release_cnt[i] = 0;
    release_4multi_cnt[0] = 0;
  }
}

void button_scan() // button value scan every 8ms
{
  GET_KEY_BITMAP();
  button_state = DATA_BUF[0] + (DATA_BUF[1] << 8);

  // BUTTON 1
  if (button_state & (1 << BUTTON_1_B)) // if power state high
  {
    if (!(lastButton_state & (1 << BUTTON_1_B))) // if rise edge
    {
      button_time[BUTTON_1_B] = 0; // start count time
      if (release_4multi_cnt[BUTTON_1_B] == 0)
        checkTripple_time[BUTTON_1_B] = 0; // start checkTripple time
    }
    else // if not rise edge
    {
      button_time[BUTTON_1_B]++;
      if (button_time[BUTTON_1_B] > DEBOUNCE_TIME)
      {
        buttonWasPress |= (1 << BUTTON_1_B); // buttonWasPress
      }
      if (button_time[BUTTON_1_B] > LONGPRESS_TIME) // buttonLongPress
      {
        buttonLongPress |= (1 << BUTTON_1_B);
        button_time[BUTTON_1_B] = 0;
        release_cnt[BUTTON_1_B] = 0;  // Not send release if having long press
      }
    }
  }
  else
  {
    if (lastButton_state & (1 << BUTTON_1_B)) // if fall edge
    {
      release_cnt[BUTTON_1_B] = 1;
      
      if (checkTripple_time[BUTTON_1_B] < TRIPPLE_TIME_MAX)
      {
        switch (release_4multi_cnt[BUTTON_1_B])
        {
        case 0:
          release_4multi_cnt[BUTTON_1_B]++;
          break;
        case 1:
          release_4multi_cnt[BUTTON_1_B]++;
          break;
        case 2:
          buttonTripplePress |= (1 << BUTTON_1_B);
          release_4multi_cnt[BUTTON_1_B] = 0;
          checkTripple_time[BUTTON_1_B] = TRIPPLE_TIME_MAX;
        default:
          break;
        }
      }
    }
  }

  if (checkTripple_time[BUTTON_1_B] < TRIPPLE_TIME_MAX)
    checkTripple_time[BUTTON_1_B]++;
  else {
    release_4multi_cnt[BUTTON_1_B] = 0;
  }

  if (release_cnt[BUTTON_1_B] == 1)
  {
    buttonWasReleased |= (1 << BUTTON_1_B); // buttonWasReleased
  }
  release_cnt[BUTTON_1_B] = 0;


  // BUTTON 2
  if (button_state & (1 << BUTTON_2_B)) // if power state high
  {
    if (!(lastButton_state & (1 << BUTTON_2_B))) // if rise edge
    {
      button_time[BUTTON_2_B] = 0; // start count time
      if (release_4multi_cnt[BUTTON_2_B] == 0)
        checkTripple_time[BUTTON_2_B] = 0; // start checkTripple time
    }
    else // if not rise edge
    {
      button_time[BUTTON_2_B]++;
      if (button_time[BUTTON_2_B] > DEBOUNCE_TIME)
      {
        buttonWasPress |= (1 << BUTTON_2_B); // buttonWasPress
      }
      if (button_time[BUTTON_2_B] > LONGPRESS_TIME) // buttonLongPress
      {
        buttonLongPress |= (1 << BUTTON_2_B);
        button_time[BUTTON_2_B] = 0;
        release_cnt[BUTTON_2_B] = 0;  // Not send release if having long press
      }
    }
  }
  else
  {
    if (lastButton_state & (1 << BUTTON_2_B)) // if fall edge
    {
      release_cnt[BUTTON_2_B] = 1;

      if (checkTripple_time[BUTTON_2_B] < TRIPPLE_TIME_MAX)
      {
        switch (release_4multi_cnt[BUTTON_2_B])
        {
        case 0:
          release_4multi_cnt[BUTTON_2_B]++;
          break;
        case 1:
          release_4multi_cnt[BUTTON_2_B]++;
          break;
        case 2:
          buttonTripplePress |= (1 << BUTTON_2_B);
          release_4multi_cnt[BUTTON_2_B] = 0;
          checkTripple_time[BUTTON_2_B] = TRIPPLE_TIME_MAX;
        default:
          break;
        }
      }
    }
  }
  if (checkTripple_time[BUTTON_2_B] < TRIPPLE_TIME_MAX)
    checkTripple_time[BUTTON_2_B]++;
  else {
    release_4multi_cnt[BUTTON_2_B] = 0;
  }

  if (release_cnt[BUTTON_2_B] == 1)
  {
    buttonWasReleased |= (1 << BUTTON_2_B); // buttonWasReleased
  }
  release_cnt[BUTTON_2_B] = 0;

  // save lastButton_state
  lastButton_state = button_state;
}

/*----------------------------------------------------------------------*
 * wasPressed() and wasReleased() check the button state to see if it   *
 * changed between the last two reads and return false (0) or           *
 * true (!=0) accordingly.                                              *
 * These functions do not cause the button to be read.                  *
 *----------------------------------------------------------------------*/
u16 button_wasPressed()
{
  u16 tempButtonWasPress;
  tempButtonWasPress = buttonWasPress;
  buttonWasPress = 0;
  return tempButtonWasPress;
}

u16 button_wasReleased()
{
  u16 tempButtonWasReleased;
  tempButtonWasReleased = buttonWasReleased;
  buttonWasReleased = 0;
  return tempButtonWasReleased;
}

u16 button_longPress()
{
  u16 tempButtonLongPress;
  tempButtonLongPress = buttonLongPress;
  buttonLongPress = 0;
  return tempButtonLongPress;
}

u16 button_tripplePress()
{
  u16 tempButtonTripplePress;
  tempButtonTripplePress = buttonTripplePress;
  buttonTripplePress = 0;
  return tempButtonTripplePress;
}